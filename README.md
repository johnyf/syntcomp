# Synthesis Competition Repository
                                               
* `Benchmarks2014` contains benchmarks collected for SYNTCOMP 2014 (in AIGER format)
* `Benchmarks2015` contains benchmarks collected for SYNTCOMP 2015 (in AIGER format)
* `Benchmarks2016` contains benchmarks collected for SYNTCOMP 2016 (in AIGER and TLSF formats)
* `benchmarks` contains original benchmarks from different tools in different formats                                                                       
* `ExperimentalData2014` contains the full experimental data for SYNTCOMP 2014
* `scripts` contains scripts for checking correctness of solutions (used in SYNTCOMP 2015), and for tagging AIGER files with meta-information based on competition results
* `tools` contains tools for viewing, executing or modifying benchmarks
