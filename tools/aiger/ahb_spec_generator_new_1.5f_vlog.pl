#! /usr/bin/perl -w

##############################################################################
# This script creates Verilog-checkers for the AMBA [1]
# synthesis benchmarks. The Verilog-checkers are translations of the
# benchmark files for ANZU [2] and RATSY [3] into Verilog. The
# Verilog-checkers have as input all inputs and outputs of the system to be
# synthesized. The Verilog-checkers have only one Boolean output, which
# signals an error. This error output must never go to TRUE for a correct
# implementation of the benchmarks. (This is a simple safety property.)
# The original AMBA benchmarks contain also fairness (Buechi)
# assumptions and guarantees. They are translated into safety properties as
# follows.
# - We reduce all fairness assumptions into one fairness assumption X using
#   a 'special' counting construction: we introduce one bit of memory per
#   fairness assumption to store if this assumption has already been visited.
# - We reduce all fairness guarantees into one fairness guarantee Y in the
#   same way.
# - We add one more counter Z. It is incremented whenever X visits and accepting
#   state. It is reset whenever Y visits and accepting state. It must never
#   reach a given value (otherwise the error-output will go to TRUE).
#   This enforces a certain ratio between fairness guarantee achievements and
#   fairness assumption achievements.
# [1] http://www.iaik.tugraz.at/content/research/design_verification/anzu/DAC07amba.pdf
# [2] http://www.iaik.tugraz.at/content/research/design_verification/anzu/
# [3] http://rat.fbk.eu/ratsy/
# 
# Usage: ./ahb_spec_generator_new_1.5nb_vlog.pl <num_of_masters> <fairness_ratio>
#
##############################################################################
#use strict;
use POSIX; # qw(ceil floor);

my $ip = "i_";             # the prefix for input variables
my $op = "controllable_";  # the prefix for output variables

###############################################
sub buildStateString {
    my ($state_name, $num_states, $value) = @_;
    my $result = "";

    my $bin = reverse sprintf("%b", $value);

    for(my $j = 0; $j < $num_states; $j++) {
        if(!($result eq "")) {
            $result .= "&";
        }

        my $bin_val = 0;
        if($j < length($bin)) {
            $bin_val = substr($bin, $j, 1);
        }

        if($bin_val) {
            $result .= " ".$state_name.$j." ";
        }
        else {
            $result .= " ~(".$state_name.$j.") ";
        }
    }

    return $result;
}

###############################################
sub buildHMasterString {
    my ($master_bits, $value) = @_;
    return buildStateString("${op}hmaster", $master_bits, $value);
}

###############################################
sub buildStateStringComment {
    my ($state_name, $num_states, $value, $padd_value, $add_next) = @_;
    my $result = "";

    if (! defined $add_next) {
        $add_next = "";
    }
    if(! defined $padd_value) {
        $padd_value = "0";
    }

    my $bin = reverse sprintf("%b", $value);

    for(my $j = 0; $j < $num_states; $j++) {
        if(!($result eq "")) {
            $result .= " * ";
        }

        my $bin_val = $padd_value;
        if($j < length($bin)) {
            $bin_val = substr($bin, $j, 1);
        }

        $result .= "$add_next(" . $state_name . $j . "=" . $bin_val . ")";
    }

    return $result;
}

###############################################
sub buildHMasterStringComment {
    my ($master_bits, $value) = @_;
    return buildStateStringComment("hmaster", $master_bits, $value);
}



###############################################
#                MAIN
###############################################

if(! defined($ARGV[0])) {
    print "Usage: ahb_spec_generator_new_1.5nb_vlog.pl <num_of_masters> <fairness_ratio k> \n";
    print "        num_of_masters   ... number of clients the bus can handle\n";
    print "        fairness_ratio k ... worst case ratio between fairness assumption\n";
    print "                             and fairness guarantee achievements:\n";
    print "                             if all fairness assumptions have been satisfied\n";
    print "                             k times, then all fairness guarantees have to\n";
    print "                             be fulfilled at least once.\n";
    exit;
}
my $num_masters = $ARGV[0];
my $ratio = $ARGV[1];
my $max_env_fair_count_string = sprintf("%b", $ratio);
my $nr_of_env_fair_count_bits = length($max_env_fair_count_string);
my $max_bit = $nr_of_env_fair_count_bits - 1;
my $max_env_fair_count_vstring = "${nr_of_env_fair_count_bits}'b${max_env_fair_count_string}";


my $master_bits = ceil((log $num_masters)/(log 2));
if($master_bits == 0) {
    $master_bits = 1;
}
my $master_bits_plus_one = ceil((log($num_masters+1))/(log 2));
if($master_bits == 0) {
    $master_bits = 1;
}

my @input_var_arr;
my @reg_var_arr;
my @env_trans_arr;
my @env_fair_arr;
my @sys_trans_arr;
my @sys_fair_arr;

###############################################
# INPUT_VARIABLES
###############################################

push(@input_var_arr, "${ip}hready");

for(my $i = 0; $i < $num_masters; $i++) {
    push(@input_var_arr, "${ip}hbusreq".$i);
    push(@input_var_arr, "${ip}hlock".$i);
}

push(@input_var_arr, "${ip}hburst0");
push(@input_var_arr, "${ip}hburst1");

###############################################
# OUTPUT_VARIABLES
###############################################

for(my $i = 0; $i < $master_bits; $i++) {
     push(@input_var_arr, "${op}hmaster".$i);
}

push(@input_var_arr, "${op}hmastlock");
push(@input_var_arr, "${op}nstart");
push(@input_var_arr, "${op}ndecide");
push(@input_var_arr, "${op}locked");
push(@input_var_arr, "${op}nhgrant0");

for(my $i = 1; $i < $num_masters; $i++) {
    push(@input_var_arr, "${op}hgrant".$i);
}

#busreq = hbusreq[hmaster]
push(@input_var_arr, "${op}busreq");

###############################################
# ENV_TRANSITION
###############################################
for(my $i = 0; $i < $num_masters; $i++) {
    my $comment = "// Assumption 3:\n";
    $comment .= "// G( hlock$i=1 -> hbusreq$i=1 );\n";
    push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." =  ~(~ ${ip}hlock".$i." | ${ip}hbusreq".$i.")");
}

###############################################
# SYS_TRANSITION
###############################################

# busreq = hbusreq[hmaster]
for(my $i = 0; $i < $num_masters; $i++) {
    my $hmaster = buildHMasterString($master_bits, $i);
    my $hmaster_comment = buildHMasterStringComment($master_bits, $i);
    
    my $comment = "// G($hmaster_comment -> (hbusreq$i=0 <-> busreq=0));\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(".$hmaster.")|(~${ip}hbusreq".$i." ^~ (~${op}busreq)))");
}

$comment = "// Guarantee 1:\n";
$comment .= "// G((hready=0) -> X(start=0));\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( reg_${ip}hready | ${op}nstart )");

$comment = "// G(((stateG2=1) * (start=1)) -> FALSE;\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_stateG2 & ~${op}nstart) | 0 )");

# the following bit-patterns appear: 000, 100, 010, 110, 001
# in the following situations start is NOT allowed: 100, 010, 110, 001
# in the following situations start is allowed: 000
$comment = "// G(((stateG3_0=1) * (stateG3_1=0) * (stateG3_2=0) * ((start=1))) -> FALSE);\n";
$comment .= "// G(((stateG3_0=0) * (stateG3_1=1) * (stateG3_2=0) * ((start=1))) -> FALSE);\n";
$comment .= "// G(((stateG3_0=1) * (stateG3_1=1) * (stateG3_2=0) * ((start=1))) -> FALSE);\n";
$comment .= "// G(((stateG3_0=0) * (stateG3_1=0) * (stateG3_2=1) * ((start=1))) -> FALSE);\n";
$comment .= "// all these rules can be summarized as: only in state 000, start=1 is allowed:\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  (reg_stateG3_0 | reg_stateG3_1 | reg_stateG3_2) & ~${op}nstart");


#Guarantee 4 and 5:
for(my $i = 0; $i < $num_masters; $i++) {

    my $hmaster_X = buildStateString("${op}hmaster", $master_bits, $i);
    my $hmaster_X_comment = buildStateStringComment("hmaster", $master_bits, $i, 0, "X");

    $comment = "// G( (hready=1) -> (   (hgrant" . $i . "=1) <-> (" . $hmaster_X_comment . ")  ) );\n";
    if($i == 0) {
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${ip}hready) | ( ~reg_${op}nhgrant0 ^~ (".$hmaster_X.") ) )");
    } else {
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${ip}hready) | ( reg_${op}hgrant".$i." ^~ (".$hmaster_X.") ) )");
    }
}
$comment = "// HMASTLOCK:\n";
$comment .= "// G(  (hready=1) -> (locked=0 <-> X(hmastlock=0) ) );\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${ip}hready) | (~reg_${op}locked ^~ ~${op}hmastlock) )");


#Guarantee 6.1:
#FIXME: I would be sufficient to have one formula for each bit of hmaster
#$sys_transitions .= "\n#Guarantee 6.1:\n";
for(my $i = 0; $i < $num_masters; $i++) {
    my $hmaster = buildStateString("reg_${op}hmaster", $master_bits, $i);
    my $hmaster_X = buildHMasterString($master_bits, $i);
    my $hmaster_comment = buildHMasterStringComment($master_bits, $i);
    my $hmaster_X_comment = buildStateStringComment("hmaster", $master_bits, $i, 0, "X");

    $comment = "// Master ".$i.":\n";
    $comment .= "// G( X(start=0) -> ( (" . $hmaster_comment . ") <-> (" . $hmaster_X_comment . ") ) );\n";
    push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(${op}nstart) | ( (".$hmaster.") ^~ (".$hmaster_X.")) )");
}

#Guarantee 6.2:
$comment = "// Guarantee 6.2:\n";
$comment .= "// G( ((X(start=0))) -> ( (hmastlock=1) <-> X(hmastlock=1)) );\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(${op}nstart) | ( reg_${op}hmastlock ^~ ${op}hmastlock) )");


#Guarantee 7:
#FIXME: formula can be written as
# G((decide=1  *  X(hgrant$i=1))-> (hlock$i=1 <-> X(locked=1)))
my $norequest = "";
my $norequest_comment = "";
for(my $i = 0; $i < $num_masters; $i++) {
    if($i == 0) {
        $comment = "// G( (decide=1  *  hlock$i=1  *  X(hgrant$i=1) )->X(locked=1));\n";
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(~reg_${op}ndecide & reg_${ip}hlock".$i." & ~${op}nhgrant0) | (${op}locked) )");
        $comment = "// G((decide=1  *  hlock$i=0  *  X(hgrant$i=1))->X(locked=0));\n";
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(~reg_${op}ndecide & ~reg_${ip}hlock".$i." & ~${op}nhgrant0) | (~${op}locked) )");
    } else {
        $comment = "// G( (decide=1  *  hlock$i=1  *  X(hgrant$i=1) )->X(locked=1));\n";
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(~reg_${op}ndecide & reg_${ip}hlock".$i." & ${op}hgrant".$i.") | (${op}locked) )");
        $comment = "// G((decide=1  *  hlock$i=0  *  X(hgrant$i=1))->X(locked=0));\n";
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(~reg_${op}ndecide & ~reg_${ip}hlock".$i." & ${op}hgrant".$i.") | (~${op}locked) )");
    }
    $norequest .= "~reg_${ip}hbusreq$i";
    $norequest .= " & " if ($i < $num_masters-1);
    $norequest_comment .= "hbusreq$i=0";
    $norequest_comment .= "  *  " if ($i < $num_masters-1);
}

#Guarantee 8:
#MW: Note, that this formula changes with respect to the number of grant signals
#$sys_transitions .= "\n#Guarantee 8:\n";
my $tmp_g8 = "";
for(my $i = 0; $i < $num_masters; $i++) {
    $comment = "// G( (decide=0) -> (  ((hgrant" . $i . "=0)<->X(hgrant" . $i . "=0))  ));\n";
    if($i == 0) {
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${op}ndecide) | (reg_${op}nhgrant0 ^~ ${op}nhgrant0) )");
    } else {
        push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${op}ndecide) | (~reg_${op}hgrant".$i." ^~ ~${op}hgrant".$i.") )");
    }
}
$comment = "// G((decide=0)->(locked=0 <-> X(locked=0)));\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_${op}ndecide) | (~reg_${op}locked ^~ ~${op}locked) )");


#Guarantee 10:
for(my $i = 1; $i < $num_masters; $i++) {
    $comment = "// G(((stateG10_".$i."=1) * (((hgrant".$i."=1)) * (hbusreq".$i."=0)))->FALSE);\n";
    push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(reg_stateG10_".$i." & (${op}hgrant".$i." & ~${ip}hbusreq".$i.")) | 0 )");
}
$comment = "// default master\n";
$comment .= "// G((decide=1  *  ".$norequest_comment.") -> X(hgrant0=1));\n";
push(@sys_trans_arr,$comment."assign sys_safe_err".@sys_trans_arr." =  ~( ~(~reg_${op}ndecide & (".$norequest.")) | (~${op}nhgrant0) )");



###############################################
# ENV_FAIRNESS
###############################################
$comment = "// Assumption 1: \n";
$comment .= "// G(F(stateA1=0));\n";
push(@env_fair_arr, $comment."assign env_fair".@env_fair_arr." =  ~reg_stateA1");

$comment = "// Assumption 2:\n";
$comment .= "// G(F(hready=1));\n";
push(@env_fair_arr, $comment."assign env_fair".@env_fair_arr." =  ${ip}hready");


###############################################
# SYS_FAIRNESS
###############################################
#Guarantee 2:
$comment = "// Guarantee 2:\n";
$comment .= "// G(F(stateG2=0));\n";
push(@sys_fair_arr, $comment."assign sys_fair".@sys_fair_arr." = ~reg_stateG2");

#Guarantee 3:
$comment = "// Guarantee 3:\n";
$comment .= "// G(F((stateG3_0=0)  *  (stateG3_1=0)  *  (stateG3_2=0)));\n";
push(@sys_fair_arr, $comment."assign sys_fair".@sys_fair_arr." =  (~reg_stateG3_0 & ~reg_stateG3_1 & ~reg_stateG3_2)");

#Guarantee 9:
for(my $j = 0; $j < $num_masters; $j++) {
    $comment = "// G(F((" . buildHMasterStringComment($master_bits, $j) . ")  |  hbusreq" . $j . "=0));\n";
    push(@sys_fair_arr, $comment."assign sys_fair".@sys_fair_arr." =  (".buildHMasterString($master_bits, $j).") | ~${ip}hbusreq".$j);
}


###############################################
# PRINT
###############################################

# collecting together the registers we need:
foreach (@input_var_arr) {
    if($_ ne "${ip}hburst0" && $_ ne "${ip}hburst1"){
        push(@reg_var_arr, "reg_".$_);
    }
}
push(@reg_var_arr, "reg_stateA1");
push(@reg_var_arr, "reg_stateG2");
push(@reg_var_arr, "reg_stateG3_0");
push(@reg_var_arr, "reg_stateG3_1");
push(@reg_var_arr, "reg_stateG3_2");
#Guarantee 10:
for(my $i = 1; $i < $num_masters; $i++) {
    push(@reg_var_arr, "reg_stateG10_".$i);
}
push(@reg_var_arr, "env_safe_err_happened");
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    push(@reg_var_arr, "env_fair".$f."done");
}
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    push(@reg_var_arr, "sys_fair".$f."done");
}


# print module header:
print "module amba_".$num_masters."_new_".$ratio."(";
print "\n        o_err,";
print "\n        i_clk,";
foreach (@input_var_arr) {
    print "\n        ".$_;
    if(\$_ != \$input_var_arr[-1]){
        print ",";
    }
}
print ");\n";
print "\n";

# print inputs:
print "input i_clk;\n";
foreach (@input_var_arr) {
    print "input ".$_.";\n";
}
# print outputs:
print "output o_err;\n";
print "\n";

# print registers:
foreach (@reg_var_arr) {
    print "reg ".$_.";\n";
}
print "reg [$max_bit:0] fair_cnt;\n";
print "\n";

# print wires:
my $cc=0;
foreach (@env_trans_arr) {
    print "wire env_safe_err".$cc.";\n";
    $cc++;
}
print "wire env_safe_err;\n";
print "\n";
$cc=0;
foreach (@sys_trans_arr) {
    print "wire sys_safe_err".$cc.";\n";
    $cc++;
}
print "wire sys_safe_err;\n";
print "\n";
$cc=0;
foreach (@env_fair_arr) {
    print "wire env_fair".$cc.";\n";
    $cc++;
}
print "\n";
$cc=0;
foreach (@sys_fair_arr) {
    print "wire sys_fair".$cc.";\n";
    $cc++;
}
print "wire all_env_fair_fulfilled;\n";
print "wire all_sys_fair_fulfilled;\n";
print "wire fair_err;\n";
print "wire o_err;\n";
print "\n";

# print assigns for to error signals for ENV_TRANSITION:
print "// =============================================================\n";
print "//                        ENV_TRANSITION:\n";
print "// =============================================================\n";
$cc=0;
$env_safe_err_string = "assign env_safe_err = ";
foreach (@env_trans_arr) {
    print $_.";\n\n";
    $env_safe_err_string .= "env_safe_err".$cc;
    if($cc != @env_trans_arr - 1) {
        $env_safe_err_string .= " |\n                      ";
    }
    $cc++;
}
print "// collecting together the safety error bits:\n";
print $env_safe_err_string.";\n";
print "\n";

# print assigns for to error signals for SYS_TRANSITION:
print "// =============================================================\n";
print "//                        SYS_TRANSITION:\n";
print "// =============================================================\n";
$cc=0;
$sys_safe_err_string = "assign sys_safe_err = ";
foreach (@sys_trans_arr) {
    print $_.";\n\n";
    $sys_safe_err_string .= "sys_safe_err".$cc;
    if($cc != @sys_trans_arr - 1) {
        $sys_safe_err_string .= " |\n                      ";
    }
    $cc++;
}
print "// collecting together the safety error bits:\n";
print $sys_safe_err_string.";\n";
print "\n";



# print assigns for to error signals for ENV_FAIRNESS:
print "// =============================================================\n";
print "//                          ENV_FAIRNESS:\n";
print "// =============================================================\n";
foreach (@env_fair_arr) {
    print $_.";\n\n";
}
print "assign all_env_fair_fulfilled = ";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    if($f != 0) {
      print " &\n                                ";
    }
    print "(env_fair${f}done | env_fair${f})";
}
print ";\n";

print "\n";

# print assigns for to error signals for SYS_FAIRNESS:
print "// =============================================================\n";
print "//                          SYS_FAIRNESS:\n";
print "// =============================================================\n";
foreach (@sys_fair_arr) {
    print $_.";\n\n";
}
print "assign all_sys_fair_fulfilled = ";
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    if($f != 0) {
      print " &\n                                ";
    }
    print "(sys_fair${f}done | sys_fair${f})";
}
print ";\n";
print "assign fair_err = (fair_cnt >= $max_env_fair_count_vstring);\n\n";

print "// computing the error output bit:\n";
print "assign o_err = ~env_safe_err & ~env_safe_err_happened & (sys_safe_err | fair_err);\n";
print "\n";

print "initial\n begin\n";
foreach (@reg_var_arr) {
    print "  ".$_." = 0;\n";
}
print "  fair_cnt = 0;\n";

print " end\n";
print "\n";


print "\n";
print "always @(posedge i_clk)\n";
print " begin\n";
print "   // We remember if an environment error occurred:\n";
print "   env_safe_err_happened = env_safe_err_happened | env_safe_err;\n";
print "\n";
print "   // Updating the fairness counters: \n";
print "   if(all_sys_fair_fulfilled)\n";
print "    begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "      env_fair${f}done = 0;\n";
}
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "      sys_fair${f}done = 0;\n";
}
print "      fair_cnt = 0;\n";
print "    end\n";
print "   else\n";
print "    begin\n";
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "      sys_fair${f}done = sys_fair${f}done | sys_fair${f};\n";
}
print "      if(all_env_fair_fulfilled)\n";
print "       begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "         env_fair${f}done = 0;\n";
}
print "         fair_cnt = fair_cnt + 1;\n";
print "       end\n";
print "      else\n";
print "       begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "         env_fair${f}done = env_fair${f}done | env_fair${f};\n";
}
print "       end\n";
print "    end\n";
print "\n";
print "   // Updating the automata: \n";
print "   // Automaton A1: \n";
print "   if(~reg_stateA1 & ${op}hmastlock & ~${ip}hburst0 & ~${ip}hburst1)\n";
print "    begin\n";
print "      reg_stateA1 = 1'b1;\n";
print "    end\n";
print "   else if(reg_stateA1 & ~${op}busreq)\n";
print "    begin\n";
print "      reg_stateA1 = 1'b0;\n";
print "    end\n";
print "\n";
print "   // Automaton G2: \n";
print "   if(~reg_stateG2)\n";
print "    begin\n";
print "      if(${op}hmastlock & ~${op}nstart & ~${ip}hburst0 & ~${ip}hburst1)\n";
print "       begin\n";
print "         reg_stateG2 = 1'b1;\n";
print "       end\n";
print "    end\n";
print "   else // if(reg_stateG2)\n";
print "    begin\n";
print "      if(~${op}busreq)\n";
print "       begin\n";
print "         reg_stateG2 = 1'b0;\n";
print "       end\n";
print "    end\n";
print "\n";
print "   // Automaton G3: \n";
print "   if(~reg_stateG3_0 & ~reg_stateG3_1 & ~reg_stateG3_2 & ${op}hmastlock & ~${op}nstart &  ~${ip}hburst0 & ${ip}hburst1 & ~${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b1;\n";
print "      reg_stateG3_1 = 1'b0;\n";
print "      reg_stateG3_2 = 1'b0;\n";
print "    end\n";
print "   else if(~reg_stateG3_0 & ~reg_stateG3_1 & ~reg_stateG3_2 & ${op}hmastlock & ~${op}nstart &  ~${ip}hburst0 & ${ip}hburst1 & ${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b0;\n";
print "      reg_stateG3_1 = 1'b1;\n";
print "      reg_stateG3_2 = 1'b0;\n";
print "    end\n";
print "   else if(reg_stateG3_0 & ~reg_stateG3_1 & ~reg_stateG3_2 & ${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b0;\n";
print "      reg_stateG3_1 = 1'b1;\n";
print "      reg_stateG3_2 = 1'b0;\n";
print "    end\n";
print "   else if(~reg_stateG3_0 & reg_stateG3_1 & ~reg_stateG3_2 & ${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b1;\n";
print "      reg_stateG3_1 = 1'b1;\n";
print "      reg_stateG3_2 = 1'b0;\n";
print "    end\n";
print "   else if(reg_stateG3_0 & reg_stateG3_1 & ~reg_stateG3_2 & ${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b0;\n";
print "      reg_stateG3_1 = 1'b0;\n";
print "      reg_stateG3_2 = 1'b1;\n";
print "    end\n";
print "   else if(~reg_stateG3_0 & ~reg_stateG3_1 & reg_stateG3_2 & ${ip}hready)\n";
print "    begin\n";
print "      reg_stateG3_0 = 1'b0;\n";
print "      reg_stateG3_1 = 1'b0;\n";
print "      reg_stateG3_2 = 1'b0;\n";
print "    end\n";

for(my $i = 1; $i < $num_masters; $i++) {
    print "\n";
    print "   // Automaton G10_$i: \n";
    print "   if(~reg_stateG10_$i & ~${op}hgrant$i & ~${ip}hbusreq$i)\n";
    print "    begin\n";
    print "      reg_stateG10_$i = 1'b1;\n";
    print "    end\n";
    print "   else if(reg_stateG10_$i & ${ip}hbusreq$i)\n";
    print "    begin\n";
    print "      reg_stateG10_$i = 1'b0;\n";
    print "    end\n";
}

print "\n";
print "   // Latching the previous input:\n";
foreach (@input_var_arr) {
    if($_ ne "${ip}hburst0" && $_ ne "${ip}hburst1"){
        print "   reg_".$_." =  ".$_.";\n";
    }
}

print "\n";
print " end\n";

print "endmodule\n";
