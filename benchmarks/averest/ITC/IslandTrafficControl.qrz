
// ====================================================================
//			Island Traffic Controller
// --------------------------------------------------------------------
// An island is connected via a tunnel with the mainland. Inside the
// tunnel is a single lane so that cars can either travel from the
// mainland to the island or vice versa, which is signaled by traffic
// lights on both ends of the tunnel. The number of cars that may be
// on the island is limited.
//  You may view the problem also as the control to a parking lot
// where the entrance does only have one lane.
//  The examples also shows how write conflicts can be resolved
// (consider the increment/decrement actions on the tunnel counter).
// ====================================================================

#ifndef MaxCars
#define MaxCars 2u
#endif


module IslandTrafficControl(
   event
   il_enter, il_leave,  // detected by sensors when cars enter/leave the island
   ml_enter, ml_leave,  // detected by sensors when cars enter/leave the mainland
   &il_red_light, &il_green_light, // signals controlling traffic lights on island
   &ml_red_light, &ml_green_light, // signals controlling traffic lights on mainland
   ?il_grant,?il_release,&il_use,&il_req,		// interface for island controller
   ?ml_grant,?ml_release,&ml_use,&ml_req,		// interface for mainland controller
   // counting cars in the tunnel and on the island
   &tc_inc,&tc_dec,&ic_inc,&ic_dec,
   nat[sizeOf(MaxCars+1u)]
   &tc, &ic)
implements IslandTrafficControlSpecs(il_enter, il_leave,ml_enter, ml_leave,il_red_light, il_green_light,
   ml_red_light, ml_green_light,
   il_grant,il_release,il_use,il_req,		
   ml_grant,ml_release,ml_use,ml_req,		
   tc_inc,tc_dec,ic_inc,ic_dec,
   tc, ic) {
     TrafficLightController
		(il_grant, il_release, il_enter, il_leave,
		 true,
		 il_green_light, il_red_light, il_use, il_req,
		 tc_inc, tc_dec, ic_dec);
  /*||
     TunnelAccessControl
		(il_use, il_req, ml_use, ml_req,
		 il_grant, il_release, ml_grant, ml_release,
		 tc, ic);*/
  || TrafficLightController
		(ml_grant, ml_release, ml_enter, ml_leave,
		 (ic<MaxCars),
		 ml_green_light, ml_red_light, ml_use, ml_req,
		 tc_inc, tc_dec, ic_inc);
  || Counter(tc_inc,tc_dec,tc);
  || Counter(ic_inc,ic_dec,ic);
}



spec IslandTrafficControlSpecs(event
   il_enter, il_leave,  // detected by sensors when cars enter/leave the island
   ml_enter, ml_leave,  // detected by sensors when cars enter/leave the mainland
   &il_red_light, &il_green_light, // signals controlling traffic lights on island
   &ml_red_light, &ml_green_light, // signals controlling traffic lights on mainland
   ?il_grant,?il_release,&il_use,&il_req,		// interface for island controller
   ?ml_grant,?ml_release,&ml_use,&ml_req,		// interface for mainland controller
   // counting cars in the tunnel and on the island
   &tc_inc,&tc_dec,&ic_inc,&ic_dec,
   nat[sizeOf(MaxCars+1u)]
   &tc, &ic) {
  /*AssumeTC: A (G (tc==0u -> ! il_leave & ! ml_leave)
		          &G (ic==0u -> ! il_enter)
	          );*/
  
  AssertLightSignal2:
    A G (! (il_green_light & il_red_light) & ! (ml_green_light & ml_red_light));
  AssertChangeDirTunnelEmpty:
    A G ((ml_red_light & X ml_green_light -> tc==0u) &
         (il_red_light & X il_green_light -> tc==0u));
  /*AssertCounterSignals1:
    A  (  (G (tc==0u -> ! il_leave & ! ml_leave)
          &G (ic==0u -> ! il_enter)
          )->G ((tc==0u -> ! tc_dec) & (ic==0 -> !ic_dec) & (ic==MaxCars -> !ic_inc)));
  AssertCounterSignals2:
    A  (  (G (ml_green_light -> (tc_inc==ic_inc)) &
			 (ml_green_light -> (! ic_inc)) &
			 (il_green_light -> (tc_inc==ic_dec)) &
			 (il_green_light -> (!ic_inc))
			 ));
  AssertCounterSignals3:
    A  (  (G (ic==MaxCars -> X ml_red_light)));*/
  AssertIslandNotOverCrowded :
    A  (  (G (tc==0u -> ! il_leave & ! ml_leave)
		          &G (ic==0u -> ! il_enter)
	          ) ->
	    G (ic<=MaxCars));
  AssertILFairness :
	A ( ((G (tc==0u -> ! il_leave & ! ml_leave)&
        G (tc>0u & il_red_light & ml_red_light -> F tc==0u) 
        ) -> G F (il_enter -> il_green_light)));
  AssertMLFairness :
    A ( ((G (tc==0u -> ! il_leave & ! ml_leave)&
        G (tc>0u & il_red_light & ml_red_light -> F tc==0u) 
        &G (ic==0u -> ! il_enter)
        &G (ic>0u -> F (il_enter))
        ) -> G F (ml_enter -> ml_green_light)));
  
}






// ----------------------------------------------------------------
// The following module is used for managing access to the tunnel,
// which is a classical problem for mutual exclusion of access to
// a shared resource. Note that the island is given a higher
// priority which is necessary to avoid deadlocks (otherwise the
// maximum number of cars could be reached on the island, but the
// mainland will have control over the tunnel, and thus, no car
// could leave the island).
// ----------------------------------------------------------------

module TunnelAccessControl(
event
   il_use,	// island has access to the tunnel
   il_req,	// island requests access to the tunnel
   ml_use,	// mainland has access to the tunnel
   ml_req,	// mainland requests access to the tunnel
   &il_grant,	// island is granted access to tunnel
   &il_release,	// island is told to release access of tunnel
   &ml_grant,	// mainland is granted access to tunnel
   &ml_release,	// mainland is told to release access of tunnel
nat[sizeOf(MaxCars+1)]
   tc,
   ic)
{
   loop {
      // -------------------------------------------------------
      // wait until there is a request
      // -------------------------------------------------------
      await (il_req | ml_req);
      if(il_req) {
	 // ----------------------------------------------------
         // tell mainland to release control over the tunnel
	 // ----------------------------------------------------
         while(ml_use) {
            pause;
            emit ml_release;
         }
	 // ----------------------------------------------------
	 // wait until all cars left the tunnel
	 // ----------------------------------------------------
         await immediate (tc==0u);
	 // ----------------------------------------------------
	 // and give the island control over the tunnel
	 // ----------------------------------------------------
         emit il_grant;
      } else if(ic<MaxCars) {
	 // ----------------------------------------------------
         // tell island to release control over the tunnel
	 // ----------------------------------------------------
         while(il_use) {
            pause;
            emit il_release;
         }
	 // ----------------------------------------------------
	 // wait until all cars left the tunnel
	 // ----------------------------------------------------
         await immediate (tc==0u);
	 // ----------------------------------------------------
	 // and give the island control over the tunnel
	 // ----------------------------------------------------
         emit ml_grant;
      }
   }
}


// spec TunnelAccessControl(event il_use) {
//    GrantOnlyIfRequested :
// 	A G ( ([ml_req WU (ml_req & ml_grant)] -> (ml_grant -> ml_req))
// 	    & ([il_req WU (il_req & il_grant)] -> (il_grant -> il_req)) );
//    GrantMutex :
// 	A G !(ml_grant & il_grant);
//    GrantOnlyIfTunnelEmpty :
// 	A G (ml_grant | il_grant -> (tc==0u));
// }






// --------------------------------------------------------------------
// The following module implements the traffic light controllers that
// are used at both ends of the tunnel. These modules are also responsible
// for counting the cars on the island and in the tunnel.
// --------------------------------------------------------------------

module TrafficLightController(
event
   grant,		// by TunnelAccessControl when tunnel access is granted
   release,		// by TunnelAccessControl when tunnel must be released
   enter,		// sent from sensor when car is entering the tunnel
   leave,		// sent from sensor when car is leaving the tunnel
   max_level,		// maximal number of cars on island is reached
   &green_light,	// set traffic light green
   &red_light,		// set traffic light red
   &use,		// tell that the tunnel is currently in use
   &req,		// request access of the tunnel
   &tc_inc,&tc_dec, 	// increment/decrement counter for cars in tunnel
   &ic_change)		// increment/decrement counter for cars on island
{
   loop{
	   if (use & tc>0u) emit next (use);
	   pause;
	   }
	   ||
   loop {
      // --------------------------------------------------------
      // Red light phase: cars can only leave the tunnel. If a
      // car leaves the tunnel, the tc counter is decremented.
      // During the red light phase, a request is sent if a car
      // wants to enter the tunnel. The red light phase ends if
      // the access to the tunnel is granted.
      // --------------------------------------------------------
      weak abort
         loop {
            while(!leave) {
               pause;
               emit red_light;
               if(enter) emit req;
            }
            emit tc_dec;
            while(leave) {
               pause;
               emit red_light;
               if(enter) emit req;
            }
         }
      when (!leave & grant);
      // --------------------------------------------------------
      // Green light phase: cars can only enter the tunnel. This
      // either increments or decrements the number of cars on
      // the island, depending on the side where this controller
      // is used. The number of cars in the tunnel is incremented.
      // The green light phase ends if the maximal number of cars
      // is reached (if it is the mainland controller) or if the
      // TunnelAccessController instructs the controller to release
      // the tunnel (since the other side send higher priortiy
      // requests).
      // --------------------------------------------------------
      weak abort
         loop {
            while(!enter) {
               pause;
               emit green_light;
               emit use;
	    }
	    emit ic_change;
            emit tc_inc;
            while(enter) {
               pause;
               emit green_light;
               emit use;
	    }
         }
      when(!max_level | release);
   }
}






// --------------------------------------------------------------------
// The following implements a simple counter that can increment and
// decrement its current value. Note that write conflicts that would
// occur by incrementing and decrementing at the same time are
// elegantly solved by this module.
// --------------------------------------------------------------------

module Counter(event inc,dec,nat[sizeOf(MaxCars+1)] &v) {
   loop {
      if(inc & !dec) 
         next(v) = v+1u;
      else if(!inc & dec)
         next(v) = v-1u;
      pause;
   }
}





