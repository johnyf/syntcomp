#!/bin/bash

if [ -z "$1" ];then
   cat <<EOF
Usage: $0 [cdaru] ...
    c <TAGS> <file>           Deletes a maybe existent SYNTCOMP section and creates a new one with the tags given.
    d <file>                  Deletes a SYNTCOMP section.
    a <TAGS> <file>           Appends all contents of TAGS to their corresponding lines and creates new entries if none are existent.
    r <STAGS> <file>          Removes all occurences of matching entries.
    u <UTAGS> <file>          Removes all occurences of matching entries and appeds the new ones. Equivalent to r with the patterns to delete and a with the new ones.

   ... to be continued

EOF
    exit 1
fi
mode=$1

function find_tag () { # file to search in
    res=$(grep -n '^#!SYNTCOMP$' $1 | head -n 1 | cut -d ':' -f 1)
    if [ -z "$res" ]; then
	echo "-1"
    fi
    echo $res
}

function find_end () { # $1 is file to search in, $2 is starting line
    res=$(tail -n +$2 $1 | grep -n '^#.$' | head -n 1 | cut -d ':' -f 1)
    if [ -z "$res" ]; then
	echo "No end for SYNTCOMP tag found! Exiting"
	exit 1
    fi
    echo $res
}


case $mode in
    c) # create
	
	echo "Not yet implemented"
	exit 1

	;;
    a) # append
	if [ -z "$2" ]; then
	    echo "No tags to append given!"
	    exit 1
	fi
	tags=$2
	tags="$tags,"
	if [ -z "$3" ]; then
	    echo "No file given!"
	    exit 1
	fi
	file=$3
	line=$(find_tag $file)
	echo -e ",w\nQ\n" | ed -s "$file"
	if [ "$line" -eq "-1" ]; then
	    line=$(wc -l $file | cut -d ' ' -f 1)
	    echo "no syntcomp tag found. creating one for you."
	    if ! (grep -q "^c" "$file"); then
		command=$(echo -e "$line a\nc\n#!SYNTCOMP\n#.\n.\n,w\nQ\n")
	    else
		command=$(echo -e "$line a\n#!SYNTCOMP\n#.\n.\n,w\nQ\n")
	    fi
	    ed -s $file <<< "$command" > /dev/null
	    ((line+=2))
	else
	    find_end $file $line > /dev/null
    	    ((line++))
	fi
	i=1
	while :; do
	    t=$(cut -d ',' -f $i <<< $tags)
	    if [ -z "$t" ]; then
		break
	    fi
	    t="$t="
	    tag=$(cut -d '=' -f 1 <<< $t)
	    value=$(cut -d '=' -f 2 <<< $t)
	    if [[ -z $tag || -z $value ]]; then
		echo "no value or tag given for $tag"
		exit 1
	    fi
	#    echo "Tag is \"$tag and value is \"$value"
	    #now find the tag to append or end
	    currl=$line
	    while read currlc; do
		if [[ "$currlc" =~ ^"$tag" ]]; then
    		  #   echo "match found in line $currl: $currlc"
		     echo -e "$currl s@\$@, $value@\n,w\nQ\n" | ed -s $file > /dev/null
		     break
		elif [ "$currlc" == "#." ]; then
		  #  echo "no match found, going to end of tag"
		    echo -e "$currl\ni\n$tag : $value\n.\n,w\nQ\n" | ed -s $file > /dev/null
		    break
		fi
		((currl++))
	    done <<< "$(tail -n +$line $file)"
	    
	   ((i++))
	
	done
	
	;;

    r) # remove a bunch of tags
	if [ -z "$2" ]; then
	    echo "No tags to remove given!"
	    exit 1
	fi
	tags=$2
	if [ -z "$3" ]; then
	    echo "No file given!"
	    exit 1
	fi
	file=$3
	line=$(find_tag $file)
	if [ "$line" -eq "-1" ]; then
	    echo "There is no SYNTCOMP section to remove tags from!"
	    exit 1
	else
	    find_end "$file" "$line" > /dev/null
    	    ((line++))
	fi
	i=1
	tags="$tags,"
	while :; do
	    t=$(cut -d ',' -f $i <<< $tags)	   
	    if [ -z "$t" ]; then
		break
	    fi
	    t="$t="

	 #   echo "I have \"$t\" and i is \"$i\""
	    tag=$(cut -d '=' -f 1 <<< $t)
	    pattern=$(cut -d '=' -f 2 <<< $t)
	    if [ -z "$tag" ]; then
		echo "no tag given!"
		exit 1
	    fi
	  #  echo "Tag is \"$tag\" and pattern is \"$pattern\""
	    #now find the tag remove stuff from
	    currl=$line
	    while read currlc; do
		if [[ "$currlc" =~ ^$tag ]]; then
    		   #  echo "match found in line $currl: $currlc"
		     if [ -z "$pattern" ]; then
		#	 echo "Removing whole line $currl"
			 echo -e "$currl d\n,w\nQ\n" | ed -s $file &> /dev/null
		     else
	       #	 echo "Removing every occurence of $pattern"
			 echo -e "$currl s@$pattern@@g\n$currl s@,[[:space:]]*,@,@g\n$currl s@:[[:space:]]*,[[:space:]]*@: @g\n$currl s@,[[:space:]]*\$@@\n,w\nQ\n" | ed -s $file	&> /dev/null	    
		     fi
		elif [ "$currlc" == "#." ]; then
		    break
		fi
		((currl++))
	    done <<< "$(tail -n +$line $file)"	   
	    echo -e "/^$tag\s*:\s\(,\s*\)*$/ d\n,w\nQ" | ed -s $file &> /dev/null
	    ((i++))	
	done	
	;;

    d) # delete completely
	file=$2
	tag=$(find_tag $file)
	if [[ "$tag" -eq -1 ]]; then
	    echo "There is no tag to delete!"
	    exit 1
	fi
	delta=$(find_end $file $tag)
	end=$((tag + delta - 1))
	echo -e "$tag,$end d\nw\nQ\n" | ed $file
	;;
    
    u) # update
	fixed=$(sed s/[]]/[]]/ <<< "$2" | sed s/[[]/[[]/ )
	$0 r "$fixed" "$3"
	$0 a "$2" "$3"
	;;

    *)
	echo "Incorrect run mode!"
	exit 1
esac
